#[macro_use]
extern crate diesel;
extern crate dotenv;

use diesel::r2d2::ConnectionManager;
use diesel::MysqlConnection;


type DbPool = r2d2::Pool<ConnectionManager<MysqlConnection>>;


pub mod controller;
pub mod utils;
pub mod dao;
pub mod schema;
pub mod models;

