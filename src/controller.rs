use serde::{Serialize, Deserialize};

pub mod admin;
pub mod front;

#[derive(Serialize, Debug, Deserialize)]
pub struct Res<T> {
    pub errno: i32,
    pub msg: String,
    pub data: Option<T>,
}

impl<T> Res<T> {
    pub fn new_with_error(errno: i32, msg: String) -> Res<T> {
        Res {
            errno,
            msg,
            data: None,
        }
    }

    pub fn new(data: Option<T>) -> Res<T> {
        Res {
            errno: 0,
            msg: "".to_string(),
            data,
        }
    }
}
