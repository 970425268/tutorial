
CREATE TABLE IF NOT EXISTS `types` (
  `id` varchar(40) COLLATE utf8mb4_bin NOT NULL primary key,
  `pid` varchar(40) COLLATE utf8mb4_bin DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '分类名称',
  `rank` int DEFAULT '0' COMMENT '排序编号，数字越大越靠前，默认 0',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '描述信息，用于seo',
  `keyword` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '关键词，用于seo',
  `icon` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '教程图标，教程列表的时候显示',
  `pic` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '教程图片，用于seo，分享的时候显示',
  `has_tutorial` tinyint(1) DEFAULT '0' COMMENT '分类下是否有教程，true=有',
  `hidden` tinyint(1) DEFAULT '1' COMMENT '是否隐藏，true隐藏',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='教程分类表';
